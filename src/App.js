

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";
import Home from './pages/Home';
import Error from './pages/Error';
import Success from './pages/Success';

import Headers from "./pages/Header";
import { HomeFormik2 } from "./pages/HomeFormik2";

function App() {
 
 
return ( 
    
      <div>
          <Headers></Headers>
        <Router>
        
          <Switch>
            <Route path="/error">
              <Error/>
            </Route>
            <Route path="/success">
               <Success/>
            </Route>
            <Route path="/">  <HomeFormik2 /> </Route>
          </Switch>
        </Router>
      </div>
    );

 
}
export default App;