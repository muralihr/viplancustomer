
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage'; 
import "firebase/compat/database";



var firebaseConfig = {
  apiKey: "AIzaSyDYwGHegpyVdtQSTZLTBfeSAqHD-Dcbm0E",
  authDomain: "viplanuserdb.firebaseapp.com",
  databaseURL: "https://viplanuserdb-default-rtdb.firebaseio.com",
  projectId: "viplanuserdb",
  storageBucket: "viplanuserdb.appspot.com",
  messagingSenderId: "206438031174",
  appId: "1:206438031174:web:ce6bc553028178c96a0f47",
  measurementId: "G-5WVBB6PGYX"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const projectStorage = firebase.storage();
const projectFirestore = firebase.firestore();
const timestamp = firebase.firestore.FieldValue.serverTimestamp;
export const db = firebase.database();
export { projectStorage, projectFirestore, timestamp,firebase  };


