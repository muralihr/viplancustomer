import React from "react";

import Alert from "react-bootstrap/Alert";

const Error = () => {
    return (
        <div>
            <Alert variant="danger" dismissible>
                <Alert.Heading>This email is already registered!</Alert.Heading>
                <p>
                Your application is under review.  <p></p>  Please wait to receive a confirmatiom email..
                </p>
            </Alert>
        </div>
    );
};

export default Error;
