import React from "react";

import { Form, Button, Card, Alert } from "react-bootstrap";


let renderCount = 0;

export default () => {
    renderCount++;

    return (
        <div style={{ marginBottom: 30 }}>
                <Card.Img   style={{ width: '18rem' , height: "10rm" }} variant="top" src="assets/OS_Logo_V2.png" />
    
            <p style={{ fontSize: 14, lineHeight: 1.3 }}>
            A Learning Programme on Organizational Cybernetics, Systemic Thinking and the Viplan Method
{" "}
            </p>
        </div>
    );
};
