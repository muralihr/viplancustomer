import React from "react";

import Alert from "react-bootstrap/Alert";

const Success = () => {
    return (
        <div>
            <Alert variant="success">
                <Alert.Heading>
                Registration details received!{" "}
                </Alert.Heading>
                <p>
                Once your registration is reviewed, you will receive an email to confirm your access to the website..
                </p>
                <hr />
            </Alert>
        </div>
    );
};

export default Success;
