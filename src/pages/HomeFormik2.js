import React from 'react';
import { Formik, Field, Form, ErrorMessage, } from 'formik';
import * as Yup from 'yup'; 
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row"; 
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import { useHistory } from "react-router-dom"; // version 5.2.0  
import { db } from "../firebase/config"; 
import firebase from 'firebase/compat/app'; 

function HomeFormik2() {

    let history = useHistory();
    let allNotes = [];
    db.ref("form/").on("value", (snapshot) => {
        console.log("inside snapshot");

        snapshot.forEach((snap) => {
            allNotes.push(snap.val());
        });

        //    console.log(allNotes);
    });
      

 
        return (
            <Formik
                initialValues={{
                
                    firstName: '',
                    lastName: '',
                    emailId: '',
                    password: '',
                    confirmPassword: '',
                 
                    picked: '',
                    topics:'' ,
                    methods:'' 
                    
                }}
                validationSchema={Yup.object().shape({
                     
                    firstName: Yup.string()
                        .required('First Name is required'),
                    lastName: Yup.string()
                        .required('Last Name is required'),
                        emailId: Yup.string()
                        .email('Email is invalid')
                        .required('Email is required'),
                    password: Yup.string()
                        .min(6, 'Password must be at least 6 characters')
                        .required('Password is required'),
                    confirmPassword: Yup.string()
                        .oneOf([Yup.ref('password'), null], 'Passwords must match')
                        .required('Confirm Password is required'), 
                        picked: Yup.string()
                        .required('Read Bood Entry  is required'),

                        topics: Yup.string()
                        .max(1500, 'topics desc must be at less than 1500 characters')
                        .required('Description required'),

                        methods: Yup.string()
                        .max(1500, 'methods desc must be at less than 1500 characters')
                        .required('Description required'),
                })}
                onSubmit={fields => {
                  //  alert('SUCCESS!! :-)\n\n' + JSON.stringify(fields, null, 4))



                  console.log(fields);
                  const a = firebase.firestore
                      .Timestamp.now().toDate().toString();
                  
                  var res = allNotes.filter((obj) =>
                      Object.values(obj).includes(fields.emailId)
                  );
                  if (res != null) console.log("res value " + res + fields.emailId);
                  else console.log("res value null " + res + fields.emailId);
                  const index = allNotes.findIndex((item) => item.emailId === fields.emailId);
                  console.log("found at index" + index);
                  if (index != -1) {
                      console.log("index == -1 " + index + fields.emailId);
                      history.push("/error");
                  
                  
                      return;
                  } else {
                      console.log("email  not found " + index + fields.emailId);
                  }
                  const params = {
                      timeStamp: a,
                      firstName: fields.firstName,
                      lastName: fields.lastName,
                      emailId: fields.emailId,
                      password: fields.password,
                      bookRead: fields.picked,
                      topics: fields.topics,
                      methods: fields.methods,
                  };
                  //
                  var adaRef = db.ref("form");
                  adaRef
                      .push(params)
                      .then(() => {
                          console.log("Your message was sent successfull");
                          history.push("/success");
                      })
                      .catch(() => {
                          console.log("Your message could not be sent");
                          history.push("/error");
                      });
              

                  ///

                }}
            >
                {({ errors, status, touched }) => (
                  <Form>

                  <Container>
                      <Row> 
                          <Col>
                              <Card>
                                  <Card.Header>Login Information</Card.Header>
                                  <Card.Body>
                                      <div className="form-row">              
                                          <div className="form-group col-5">
                                              <label htmlFor="firstName">First Name</label>
                                              <Field name="firstName" type="text" placeholder="First Name" className={ 'form-control' + (errors.firstName && touched.firstName ? ' is-invalid' : '')} />
                                              <ErrorMessage name="firstName" component="div" className="invalid-feedback" />
                                          </div>
                                          <div className="form-group col-5">
                                              <label htmlFor="lastName">Last Name</label>
                                              <Field name="lastName" type="text" placeholder="Last Name" className={ 'form-control' + (errors.lastName && touched.lastName ? ' is-invalid' : '')} />
                                              <ErrorMessage name="lastName" component="div" className="invalid-feedback" />
                                          </div>
                                      </div>
                                      <div className="form-group">
                                          <label htmlFor="email">Email</label>
                                          <Field name="emailId" type="text" placeholder="Email Address" className={ 'form-control' + (errors.emailId && touched.emailId ? ' is-invalid' : '')} />
                                          <ErrorMessage name="emailId" component="div" className="invalid-feedback" />
                                      </div>
                                      <div className="form-row">
                                          <div className="form-group col">
                                              <label htmlFor="password">Password</label>
                                              <Field name="password" type="password" placeholder="Password" className={ 'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                                              <ErrorMessage name="password" component="div" className="invalid-feedback" />
                                          </div>
                                          <div className="form-group col">
                                              <label htmlFor="confirmPassword">Confirm Password</label>
                                              <Field name="confirmPassword" type="password" placeholder=" Confirm Password" className={ 'form-control' + (errors.confirmPassword && touched.confirmPassword ? ' is-invalid' : '')} />
                                              <ErrorMessage name="confirmPassword" component="div" className="invalid-feedback" />
                                          </div>
                                      </div>
                                  </Card.Body>
                              </Card> 
                          </Col> 
                      </Row>
              
                      <Col>
              
                          <Card>
                              <Card.Header>Prerequisites </Card.Header>
                              <Card.Body>
                                  <div id="my-radio-group">Have you completed reading the book "Organizational Systems: Managing Complexity with the Viable SystemModel" by Raul Espejo & Alfonso Reyes? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      <p></p>
                                  </div>
                                  <div role="group" aria-labelledby="my-radio-group" className={ 'form-control' + (errors.picked && touched.picked ? ' is-invalid' : '')}>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      <label>
                                          <Field type="radio" name="picked" value="YES" /> YES
                                      </label>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      <label>
                                          <Field type="radio" name="picked" value="NO" /> NO
                                      </label> 
                                      <ErrorMessage name="picked" component="div" className="invalid-feedback" />
                                  </div>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  <div className="form-row">
                                      <div className="form-group col">
              
                                          {" "} Describe briefly (in English or Spanish) what topics in the book you need more clarity on, or what areas you want to understand better :
                                          <p></p>{" "}
                                          <Field name="topics" component="textarea" rows="5" style={{ width: "100%" }} className={ 'form-control' + (errors.topics && touched.topics ? ' is-invalid' : '')} />
                                          <ErrorMessage name="topics" component="div" className="invalid-feedback" />
                                      </div>
                                  </div>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  <div className="form-row">
                                      <div className="form-group col">
              
                                          {" "} Describe briefly (in English or Spanish) a situation that you are trying to apply the methods described in the book to :
                                          <p></p>{" "}
                                          <Field name="methods" component="textarea" rows="5" style={{ width: "100%" }} className={ 'form-control' + (errors.methods && touched.methods ? ' is-invalid' : '')} />
                                          <ErrorMessage name="methods" component="div" className="invalid-feedback" />
                                      </div>
                                  </div>
                               
                                  <p></p>
                                  <div className="form-group">
                                      <button type="submit" className="btn btn-primary mr-2">Request Access</button> &nbsp;&nbsp;&nbsp;
                                      <button type="reset" className="btn btn-secondary">Reset</button>
                                  </div>
              
                              </Card.Body>
                          </Card>
                      </Col>
                  </Container>  
              </Form>
                )}
            </Formik>
        )
    }
 

export { HomeFormik2 }; 