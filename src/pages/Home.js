import logo from "../logo.svg";
import "../App.css";
import useFirestore from "../hooks/useFirestore";
import { db } from "../firebase/config";
import { ErrorMessage } from "@hookform/error-message";
import { useForm } from "react-hook-form";
import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Alert from "react-bootstrap/Alert";
import Col from "react-bootstrap/Col";
import { Controller, SubmitHandler } from "react-hook-form";

import firebase from 'firebase/compat/app';
import { useHistory } from "react-router-dom"; // version 5.2.0

function Home() {
    const [inputValue, setInputValue] = useState("");
    const [users, setUsers] = useState([]);
    const [curr , setCurr] = useState('');

// Function to get time and date
const getDate = () => {
  
  
}
    let history = useHistory();
    const {
        register,
        formState: { errors },
        handleSubmit,
    } = useForm({
        criteriaMode: "all",
    });
    const { docs } = useFirestore("images");
    console.log(docs);
    let allNotes = [];
    db.ref("form/").on("value", (snapshot) => {
        console.log("inside snapshot");

        snapshot.forEach((snap) => {
            allNotes.push(snap.val());
        });

        //    console.log(allNotes);
    });

    const onSubmit = (data) => {
        console.log(data);
        const a = firebase.firestore
       .Timestamp.now().toDate().toString();
        
        var res = allNotes.filter((obj) =>
            Object.values(obj).includes(data.emailId)
        );
        if (res != null) console.log("res value " + res + data.emailId);
        else console.log("res value null " + res + data.emailId);    
        const index = allNotes.findIndex((item) => item.emailId === data.emailId);

        console.log("found at index" + index);
        if (index != -1) {
            console.log("index == -1 " + index + data.emailId);
            history.push("/error");
            return;
        } else {
            console.log("index == -1 " + index + data.emailId);
        }

        console.log("radio value" + inputValue);
        const params = {
            timeStamp : a,
            firstName: data.firstName,
            lastName: data.lastName,
            emailId: data.emailId,
            bookRead: inputValue,
            topics: data.topics,
            methods: data.methods,
        };
        var adaRef = db.ref("form");
        adaRef
            .push(params)
            .then(() => {
                console.log("Your message was sent successfull");
                history.push("/success");
            })
            .catch(() => {
                console.log("Your message could not be sent");
                history.push("/error");
            });
    };
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Container>
                <Row>
                    <Col>
                        First Name{" "}
                        <input
                            placeholder="First Name"
                            {...register("firstName", { required: true })}
                        />
                        {errors.firstName?.type === "required" &&
                            "First name is required"}
                        <p></p>
                        Last Name
                        <input
                            placeholder="Last Name"
                            {...register("lastName", { required: true })}
                        />
                        {errors.lastName && "Last name is required"}
                        <p></p>
                        Email Id
                        <input
                            placeholder="Email Address"
                            {...register("emailId", {
                                required: "Email Id is required.",
                                pattern: {
                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                    message: "Enter a valid e-mail address",
                                },
                                minLength: {
                                    value: 1,
                                    message: "Email Id must exceed 1 characters",
                                },
                            })}
                        />
                        <ErrorMessage
                            errors={errors}
                            name="emailId"
                            render={({ messages }) => {
                                console.log("messages", messages);
                                return messages
                                    ? Object.entries(messages).map(
                                          ([type, message]) => (
                                              <p key={type}>{message}</p>
                                          )
                                      )
                                    : null;
                            }}
                        />
                    </Col>
                    <Col></Col>
                </Row>
                <Row>
                    <Col>
                  
                        <Row>
                        Have you completed reading the book "Organizational Systems:
                        Managing Complexity with the Viable SystemModel" by Raul
                        Espejo & Alfonso Reyes?
                            <p></p>
                            <Col>  <label for="html">YES</label>
                                <input
                                    type="radio"
                                    id="html"
                                    name="fav_language"
                                    value="YES"
                                    value="YES"
                                    onClick={() => setInputValue("YES")}
                                ></input></Col>
                            <Col> <label for="css">NO</label>
                                <input
                                    type="radio"
                                    id="css"
                                    name="fav_language"
                                    value="NO"
                                    onClick={() => setInputValue("NO")}
                                ></input></Col>
                            
                            <div>
                              
                               
                            </div>
                        </Row>
                        <Row>
                            Describe briefly (in English or Spanish) what topics in
                            the book you need more clarity on, or what areas you
                            want to understand better : <p></p>{" "}
                            <textarea
                                placeholder="Details here"
                                rows="4"
                                cols="50"
                                name="comment"
                                {...register("topics", { required: true })}
                            ></textarea>
                            {errors.textareadetails1?.type === "required" &&
                                "Topic Details is required"}
                        </Row>
                        <Row>
                            {" "}
                            Describe briefly (in English or Spanish) a situation
                            that you are trying to apply the methods described in
                            the book to : <p></p>
                            <textarea
                                placeholder="Details here"
                                rows="4"
                                cols="50"
                                name="comment"
                                {...register("methods", { required: true })}
                            >
                                
                            </textarea>
                            {errors.textareadetails1?.type === "required" &&
                                "Situation Details  is required"}
                        </Row>
                    </Col>
                  
                </Row>
            </Container>
            <Container>
            <Col>  </Col>
            <Col>
                  
                <Row>
                <input type="submit"    value="Request Access" />{" "}
                </Row>
                
                
            </Col>
            <Col></Col>
            </Container>
        </form>
    );
}
export default Home;
